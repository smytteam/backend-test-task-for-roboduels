### Application description
This application implements several api methods for register and verify users in system.
####Introduction
Please rename settings/local_settings.py.sample to settings/local_settings.py and set values for Twilio account as local_settings.py example has.

#### API
Description for all api methods in this app:
Required http **method**: POST, response: {success: True/False}, if errors exist they also will be add to response

**API methods:**
1. /api/register/ - first step, register user by username, email and password. Response also will be have user_id value of created user. You can use it in next requests. **Parameters**: 'username', 'email', 'password'. 
2. /api/register-phone/ - second step, this and next step must have user_id as parameter in request method, you also must send country_code and phone number in your request. After it, backend send sms with verification code to user. **Parameters**: user, code, phone.
3. /api/verify-phone/ - third step, verification sms code, you must send user_id, verification code. **Parameters**: user, code.
4. /api//auth-user/ - standard user authentication is implemented by this app api. **Parameters**: username, password. 
    