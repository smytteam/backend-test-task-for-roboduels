from django import forms
from django.conf import settings

from otp.models import UserOtpCode
from user_auth.models import CustomUser


class PhoneForm(forms.Form):
    user = forms.ModelChoiceField(queryset=CustomUser.objects.all())
    code = forms.CharField(label='Country code', max_length=5)
    phone = forms.CharField(label='Phone Number', max_length=10)


class SmsCodeForm(forms.Form):
    code = forms.CharField(label='SMS code', max_length=settings.OTP_CODE_LENGTH)
    user = forms.ModelChoiceField(queryset=CustomUser.objects.all())

    def clean(self):
        cleaned_data = super().clean()
        code = cleaned_data.get('code')
        user = cleaned_data.get('user')
        is_valid_code = UserOtpCode.verify_code(user=user, code=code)
        if is_valid_code:
            user.sms_verified = True
            user.save(update_fields=['sms_verified', ])
            UserOtpCode.delete_user_code(user)
        else:
            raise forms.ValidationError('Sms code is invalid or expired')


class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'password')
