from django.urls import path

from .views import RegisterPhoneView
from .views import RegisterUserView
from .views import UserAuthView
from .views import VerifySmsCodeView

urlpatterns = [
    path('register-phone/', RegisterPhoneView.as_view(), name='register-phone'),
    path('verify-phone/', VerifySmsCodeView.as_view(), name='verify-phone'),
    path('register/', RegisterUserView.as_view(), name='register-user'),
    path('auth-user/', UserAuthView.as_view(), name='auth-user'),
]
