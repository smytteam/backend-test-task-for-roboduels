from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from otp.generators import OTPGenerator
from otp.models import UserOtpCode
from test_sms_task.utils import send_sms
from .forms import PhoneForm
from .forms import SmsCodeForm
from .forms import UserRegisterForm


class BaseResponseView(View):
    """
    Base class for api methods.
    """
    http_method_names = ['post', ]
    form_class = None

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def _process_form(self, form, answer):
        """We can add additional form processing."""
        pass

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        status = 201
        answer = {'success': True}
        if form.is_valid():
            self._process_form(form, answer)
        else:
            answer = {'success': False, 'errors': form.errors}
            status = 400
        return JsonResponse(answer, status=status)


class RegisterPhoneView(BaseResponseView):
    form_class = PhoneForm

    def _process_form(self, form, answer):
        user = form.cleaned_data.get('user')
        phone = form.cleaned_data.get('phone')
        country_code = form.cleaned_data.get('code')
        phone = "{}{}".format(country_code, phone)

        code = OTPGenerator().generate()
        if settings.DEBUG:
            print(code)
        UserOtpCode.create_or_update_sms_code(user, code)
        message = 'Your verification code is: {}'.format(code)

        sms_answer = send_sms(phone, message)
        if sms_answer.get('errors') and not sms_answer.get('sid'):
            answer['success'] = False
            answer['errors'] = sms_answer.get('errors')


class VerifySmsCodeView(BaseResponseView):
    form_class = SmsCodeForm


class RegisterUserView(BaseResponseView):
    form_class = UserRegisterForm

    def _process_form(self, form, answer):
        user = form.save()
        answer['user_id'] = user.pk


class UserAuthView(BaseResponseView):
    form_class = AuthenticationForm
