from django.contrib import admin

from .models import UserOtpCode

admin.site.register(UserOtpCode)
