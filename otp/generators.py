import random

from django.conf import settings


class OTPGenerator:
    """
    Simple otp number generator.
    Generated code will be consists of digits, only.
    """
    def __init__(self):
        self.otp = ""

    def generate(self):
        self.otp = ""
        for i in range(settings.OTP_CODE_LENGTH):
            self.otp += str(random.randint(0, 9))
        return self.otp
