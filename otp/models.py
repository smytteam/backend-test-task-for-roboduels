from datetime import datetime, timedelta

from django.conf import settings
from django.db import models


class UserOtpCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='User')
    code = models.CharField('Code', max_length=settings.OTP_CODE_LENGTH)
    date_created = models.DateTimeField('Date created/updated', auto_now=True)

    class Meta:
        verbose_name = 'User OTP code'
        verbose_name_plural = 'User OTP codes'

    def __str__(self):
        return self.code

    @classmethod
    def verify_code(cls, user, code):
        """
        Verify sms code
        Sms Code must be in this table and not to be expired
        :param user: user from request
        :param code: sms user code
        :return: valid or not user sms code
        """
        date_check = datetime.now() - timedelta(minutes=settings.OTP_CODE_TIME_LIFE)
        is_valid_code = cls.objects.filter(user=user, code=code, date_created__gte=date_check).exists()
        return is_valid_code

    @classmethod
    def delete_user_code(cls, user):
        """
        Delete all user sms codes from table if exist
        :param user: user from request
        :return: None
        """
        cls.objects.filter(user=user).delete()

    @classmethod
    def create_or_update_sms_code(cls, user, code):
        """
        If user already exists we update sms code for him
        another we create user with new generated sms code
        :param user: user from request
        :param code: new generated sms code
        :return: None
        """
        otp_user = cls.objects.filter(user=user).first()
        if otp_user:
            otp_user.code = code
            otp_user.save()
        else:
            cls.objects.create(user=user, code=code)