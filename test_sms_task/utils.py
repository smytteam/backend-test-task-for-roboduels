from django.conf import settings
from twilio.base.exceptions import TwilioRestException

from twilio.rest import Client


def send_sms(phone, body):
    account_sid = settings.TWILIO_ACCOUNT_SID
    auth_token = settings.TWILIO_AUTH_TOKEN
    send_from = settings.TWILIO_SEND_FROM
    client = Client(account_sid, auth_token)
    answer = {}
    try:
        message = client.messages \
            .create(
            body=body,
            from_=send_from,
            to=phone
        )
    except TwilioRestException as e:
        answer['errors'] = str(e)
        message = None
    if message and message.sid:
        answer['sid'] = message.sid
    return answer
