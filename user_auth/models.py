from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    sms_verified = models.BooleanField('Is login verified', default=False)
